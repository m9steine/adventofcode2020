package day3;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Locale.IsoCountryCode;

import utils.InputUtils;

public class Dec3
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day3/input.txt");
        char[][] grid = initGrid(input);
        System.out.println("Part1 - #Trees:" + checkPosition(0, 0, grid, 0, 3, 1));
         System.out.println("Part2 - #Trees:" + (
         checkPosition(0, 0, grid, 0, 1, 1)*
         checkPosition(0, 0, grid, 0, 3, 1)*
         checkPosition(0, 0, grid, 0, 5, 1)*
         checkPosition(0, 0, grid, 0, 7, 1)*
         checkPosition(0, 0, grid, 0, 1, 2)));

    }

    public static long checkPosition(int startX, int startY, char[][] grid, long countTrees, int stepX, int stepY)
    {
        startX = startX >= grid[startY].length ? startX % grid[startY].length : startX;
        if (grid[startY][startX] == '#')
        {
            countTrees++;
        }
        if (startY >= grid.length - 1)
        {
            return countTrees;
        }

        return checkPosition(startX + stepX, startY + stepY, grid, countTrees, stepX, stepY);

    }

    static char[][] initGrid(List<String> input)
    {
        int lineLength = input.get(0).length();
        char[][] grid = new char[input.size()][lineLength];
        int lineNumber = 0;

        for (String line : input)
        {
            for (int ind = 0; ind < line.length(); ind++)
            {
                grid[lineNumber][ind] = line.charAt(ind);
            }
            lineNumber++;
        }
        return grid;

    }

    public static void printGrid(char[][] grid)
    {
        for (int ind = 0; ind < grid.length; ind++)
        {
            for (int jnd = 0; jnd < grid[ind].length; jnd++)
            {
                System.out.print(grid[ind][jnd]);
            }
            System.out.println();
        }
    }

}
