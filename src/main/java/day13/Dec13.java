package day13;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import utils.InputUtils;

public class Dec13
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day13/input.txt");
        long timestampOfArrival = Long.parseLong(input.get(0));
        List<Bus> buses = initBuses(input.get(1));
        getNextBus(buses, timestampOfArrival);
        System.out.println("Part1- " + getNextBus(buses, timestampOfArrival));
        System.out.println("Part2- " + getFirstTimestampMatching(buses));
    }

    public static long getFirstTimestampMatching(List<Bus> buses)
    {
        long timestamp = 100000000000000L;
//        long timestamp = 0;
        while (true)
        {
            long tempTime = buses.stream().mapToLong(v->v.nextDepart).min().getAsLong();
            List<Bus> current = buses.stream().filter(bus -> bus.nextDepart == tempTime).collect(Collectors.toList());
            if (current.size() > 0)
            {
                for (Bus temp : current)
                {
                    temp.lastDepart = temp.nextDepart;
                    temp.nextDepart += temp.id;
                }
            }
//            if (timestamp == 1068781 && checkBuses(buses))
            if (timestamp % 100000000L == 0)
            {
                System.out.println("hjgfhjf---" + timestamp);
            }
                if (checkBuses(buses))
            {
                return tempTime;

            }
            timestamp = tempTime;
        }
//        return 0;
    }
    public static boolean checkBuses(List<Bus> buses)
    {
        boolean result = true;
//        long min = buses.stream().mapToLong(v->v.getNextDepart()).so;
//        long max = buses.stream().mapToLong(v->v.getNextDepart()).max().getAsLong();
        buses = buses.stream().sorted(Comparator.comparingLong(Bus::getMinutesAfterTimestamp)).collect(Collectors.toList());

        Bus current =buses.get(0);
        for (int index = 1; index < buses.size();index++)
        {
            Bus next = buses.get(index);
            if (current.lastDepart + next.minutesAfterTimestamp != next.nextDepart)
            {
                result = false;
            }
        }
        long maxMinutesToWait = buses.stream().mapToLong(v->v.minutesAfterTimestamp).max().getAsLong();

//        if (max - min <= maxMinutesToWait)
//        {
//            return true;
//        }
        return result;
    }

    public static long getNextBus(List<Bus> buses, long arrival)
    {
        long timestamp = 0;
        while (timestamp <= arrival)
        {
            long tempTime = timestamp;
            List<Bus> current = buses.stream().filter(bus -> bus.nextDepart == tempTime).collect(Collectors.toList());
            if (current.size() > 0)
            {
                for (Bus temp : current)
                {
                    temp.lastDepart = temp.nextDepart;
                    temp.nextDepart += temp.id;
//                    if (temp.id == 59)
//                    {
//                        System.out.println("--");
//                    }

                }
            }
            if (timestamp == arrival)
            {
                long nextArrivalTimestamp = buses.stream().mapToLong(v -> v.getNextDepart()).min().getAsLong();
                Bus bus =
                    buses.stream().filter(entitiy -> entitiy.nextDepart == nextArrivalTimestamp).findFirst().get();
                return (nextArrivalTimestamp - arrival) * bus.id;
            }
            timestamp++;
        }
        return 0;
    }

    public static List<Bus> initBuses(String schedule)
    {
        List<Bus> tempBuses = new ArrayList<Dec13.Bus>();
        int index = 0;
        for (String bus : schedule.split(","))
        {
            if (!bus.equals("x"))
            {
                tempBuses.add(new Bus(Long.parseLong(bus), index));
            }
            index++;
        }
        return tempBuses;
    }

    public static class Bus
    {
        long id;
        long lastDepart;
        long nextDepart;
        long minutesAfterTimestamp;

        public long getMinutesAfterTimestamp()
        {
            return minutesAfterTimestamp;
        }

        Bus(long id, long index)
        {
            this.id = id;
            this.lastDepart = id;
            this.nextDepart = id*9999999999999L;
//            this.nextDepart = id;
            this.minutesAfterTimestamp = index;
        }

        public long getNextDepart()
        {
            return nextDepart;
        }

        public void setNextDepart(long nextDepart)
        {
            this.nextDepart = nextDepart;
        }

        public long getLastDepart()
        {
            return lastDepart;
        }

        public void setLastDepart(long lastDepart)
        {
            this.lastDepart = lastDepart;
        }
    }

}
