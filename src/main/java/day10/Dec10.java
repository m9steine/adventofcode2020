package day10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import utils.InputUtils;

public class Dec10
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day10/input.txt");
        System.out.println(initInput(input));
        System.out.println(checkJolts(initInput(input)));
        countArrangemnts(initInput(input));
    }

    public static void countArrangemnts(List<Integer> input)
    {
        input.add(input.get(input.size()-1)+3);
        input.add(0, 0);
        long[] result  = new long[input.size()];
        result[0] = 1;

        for(int index = 0; index < input.size(); index++)
        {
            if (index >= 1)
            {
                if (input.get(index) - input.get(index-1) == 1 || input.get(index) - input.get(index-1) == 2 || input.get(index) - input.get(index-1) == 3)
                {
                    result[index] += result[index-1];
                }
            }

            if(index >= 2)
            {
                if (input.get(index) - input.get(index-2) == 2 || input.get(index) - input.get(index-2) == 3)
                {
                    result[index] += result[index-2];
                }
            }

            if (index >= 3)
            {
                if (input.get(index) - input.get(index-3) == 3)
                {
                    result[index] += result[index-3];
                }
            }
        }
    }

    public static long checkJolts(List<Integer> input)
    {
        // lost the calculation, due making to less commits :D

        return 0L;
    }


    public static List<Integer> initInput(List<String> input)
    {
        List<Integer> output = new ArrayList<Integer>();
        for (String line : input)
        {
            output.add(Integer.parseInt(line));
        }
        return output.stream().sorted().collect(Collectors.toList());
    }

}
