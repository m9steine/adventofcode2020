//package utils;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//
//
//public class InputUtils
//{
//    private final String URL_PREFIX = "https://adventofcode.com/2019/day/";
//    private final String URL_SUFFIX = "/input";
//
//    public List<String> getInput (String day)
//    {
//        List<String> input = new ArrayList<String>();
//            try
//            {
//                String url = URL_PREFIX + day + URL_SUFFIX;
//                Document doc = Jsoup.connect(url).get();
//                String text = doc.body().text();
//
//                System.out.println(text);
//            }
//            catch (IOException e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//
//        return input;
//
//    }
//
//}

package utils;

import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class InputUtils {

    public List<String> getInput(String fileName)  {

        URL resource = ClassLoader.getSystemResource(fileName);
        try {
            URI uri =resource.toURI();
            Path path = Paths.get(uri);
            return Files.lines(path).collect(Collectors.toList());
        } catch (Exception e) {
            System.exit(1);
        }

        return null;
    }

}
