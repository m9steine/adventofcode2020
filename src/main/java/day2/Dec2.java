package day2;

import java.util.List;

import utils.InputUtils;

public class Dec2
{

    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day2/input.txt");

        System.out.println("valid password - Part1: " + getNumValidPasswords(input));
        System.out.println("valid password - Part2: " + getNumValidPasswordsPartTwo(input));

    }

    public static int getNumValidPasswords(List<String> input)
    {
        int validPasswords = 0;
        for (String line : input)
        {
            PasswordCheck entity = fillPasswordCheck(line);
            int count = countCheckInPasswordPartTwo(entity.first, entity.second, entity.check.charAt(0), entity.password);

            if (entity.first <= count && count <= entity.second)
            {
                validPasswords++;
            }
        }
        return validPasswords;

    }

    public static int countCheckInPassword(char checkPhrase, String password)
    {
        int count = 0;
        for (int ind = 0; ind < password.length(); ind++)
        {
            char test = password.charAt(ind);
            if (test == checkPhrase)
            {
                count++;
            }
        }
        return count;
    }

    public static int getNumValidPasswordsPartTwo(List<String> input)
    {
        int validPasswords = 0;
        for (String line : input)
        {
            PasswordCheck entity = fillPasswordCheck(line);
            int count = countCheckInPasswordPartTwo(entity.first, entity.second, entity.check.charAt(0), entity.password);

            if (count == 1)
            {
                validPasswords++;
            }
        }
        return validPasswords;
    }

    public static int countCheckInPasswordPartTwo(int first, int second, char checkPhrase, String password)
    {
        int count = 0;
        if (checkPhrase == password.charAt(first - 1))
        {
            count++;
        }
        if (checkPhrase == password.charAt(second - 1))
        {
            count++;
        }
        return count;
    }

    public static PasswordCheck fillPasswordCheck(String line)
    {
        return new PasswordCheck(
            Integer.parseInt(line.substring(0, line.indexOf("-"))),
            Integer.parseInt(line.substring(line.indexOf("-") + 1, line.indexOf(" "))),
            line.substring(line.indexOf(":") - 1, line.indexOf(":")),
            line.substring(line.indexOf(":") + 2, line.length()));
    }

    public static class PasswordCheck
    {
        int first;
        int second;
        String check;
        String password;

        public PasswordCheck(int first, int second, String check, String password)
        {
            this.first = first;
            this.second = second;
            this.check = check;
            this.password = password;
        }
    }

}
