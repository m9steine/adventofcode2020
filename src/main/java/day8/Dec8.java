package day8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.InputUtils;

public class Dec8
{
    private static InputUtils helper = new InputUtils();
    private static int accumulator = 0;
    private static int index = 0;

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day8/input.txt");
        System.out.println("Part1 - Acc: " + calculateAccumulator(initOperationList(input)));
        int countOperationsNopJmp = countOperations(initOperationList(input));
        for (int index = 1; index <= countOperationsNopJmp; index++)
        {
            int test = index;
            new Thread(() -> {
                System.out.println("Part2 - Acc: " + calculateAccumulatorPart2(initOperationList(input), test) + "------" + test);
            }).start();
        }
    }

    public static int calculateAccumulator(List<Operation> operations)
    {
        for (int index = 0; index < operations.size(); index++)
        {
            Operation iter = operations.get(index);
            if (iter.visited)
            {
                return accumulator;
            }
            switch (iter.name)
            {
                case "nop":
                    iter.visited = true;
                    break;
                case "acc":
                    iter.visited = true;
                    accumulator += iter.value;
                    break;
                case "jmp":
                    iter.visited = true;
                    index += iter.value - 1;
                    break;
                default:
                    break;
            }
        }
        return 0;
    }

    public static int calculateAccumulatorPart2(List<Operation> operations, int indexOp)
    {
        int accumulator = 0;
        int numOpNopJmp = 0;
        for (int index = 0; index < operations.size(); index++)
        {
            Operation iter = operations.get(index);
            if (iter.name.equals("nop") || iter.name.equals("jmp"))
            {
                numOpNopJmp++;
            }
            if (numOpNopJmp == indexOp)
            {
                switch (iter.name)
                {
                    case "nop":
                        iter.name = "jmp";
                        break;
                    case "jmp":
                        iter.name = "nop";
                        break;
                }
            }
            switch (iter.name)
            {
                case "nop":
                    iter.visited = true;
                    break;
                case "acc":
                    iter.visited = true;
                    accumulator += iter.value;
                    break;
                case "jmp":
                    iter.visited = true;
                    index += iter.value - 1;
                    break;
                default:
                    break;
            }
        }
        return accumulator;
    }

    public static int countOperations(List<Operation> operations)
    {
        return (int) operations.stream().filter(op -> op.name.equals("nop") || op.name.equals("jmp")).map(v -> v)
            .count();
    }

    public static List<Operation> initOperationList(List<String> input)
    {
        List<Operation> operations = new ArrayList<Operation>();
        for (String line : input)
        {
            String name = line.split(" ")[0];
            int value = Integer.parseInt(line.split(" ")[1]);
            operations.add(new Operation(name, value));
        }
        return operations;
    }

    public static class Operation
    {
        String name;
        int value;
        boolean visited = false;
        boolean changed = false;
        boolean touched = false;

        Operation(String name, int value)
        {
            this.name = name;
            this.value = value;
        }
    }

}
