package day4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.InputUtils;

public class Dec4
{
    private static InputUtils helper = new InputUtils();
    public static int passportId = 0;

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day4/input.txt");
        System.out.println("+valid passwords - Part1: " + getValidPassports(getPassports(input)).size());
        System.out.println("+valid passwords - Part2: " + getValidPassportsPartTwo(getValidPassports(getPassports(input))).size());
    }

    /*
     * byr (Birth Year) - four digits; at least 1920 and at most 2002.
     * iyr (Issue Year) - four digits; at least 2010 and at most 2020.
     * eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
     * hgt (Height) - a number followed by either cm or in:
     * - If cm, the number must be at least 150 and at most 193.
     * - If in, the number must be at least 59 and at most 76.
     * hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
     * ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
     * pid (Passport ID) - a nine-digit number, including leading zeroes.
     * cid (Country ID) - ignored, missing or not.
     */
    public static List<Passport> getValidPassports(List<Passport> passports)
    {
        /*
         * byr (Birth Year) iyr (Issue Year) eyr (Expiration Year) hgt (Height) hcl (Hair Color) ecl (Eye Color) pid
         * (Passport ID) cid (Country ID) - optional
         */
        String[] validPassportFields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};
        List<String> requiredFields = List.of(validPassportFields);
        List<Passport> validPassports = new ArrayList<Dec4.Passport>();
        for (Passport passport : passports)
        {
            boolean isValid = true;
            for (String field : requiredFields)
            {
                isValid = isValid && passport.getCredentials().keySet().contains(field);
            }
            if (isValid)
            {
                validPassports.add(passport);
            }
        }
        return validPassports;
    }

    public static List<Passport> getValidPassportsPartTwo(List<Passport> passports)
    {
        /*
         * byr (Birth Year) - four digits; at least 1920 and at most 2002.
         * iyr (Issue Year) - four digits; at least 2010 and at most 2020.
         * eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
         * hgt (Height) - a number followed by either cm or in:
         * * If cm, the number must be at least 150 and at most 193.
         * * If in, the number must be at least 59 and at most 76.
         * hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
         * ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
         * pid (Passport ID) - a nine-digit number, including leading zeroes.
         * cid (Country ID) - ignored, missing or not.
         */
        String[] eyeColors = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"};
        List<String> validEyeColor = List.of(eyeColors);
        List<Passport> validPassports = new ArrayList<Dec4.Passport>();
        for (Passport passport : passports)
        {
            boolean isValid = true;
            for (String field : passport.getCredentials().keySet())
            {
                isValid = isValid && passport.getCredentials().keySet().contains(field);
                if (isValid)
                {
                switch (field)
                {
                    case "byr":
                        int birthYear = Integer.parseInt(passport.getCredentials().get(field));
                        isValid = isValid && passport.getCredentials().get(field).length() == 4;
                        if (birthYear < 1920 || birthYear > 2002)
                        {
                            isValid = false;
                        }
                        break;

                    case "iyr":
                        int issueYear = Integer.parseInt(passport.getCredentials().get(field));
                        isValid = isValid && passport.getCredentials().get(field).length() == 4;
                        if (issueYear < 2010 || issueYear > 2020)
                        {
                            isValid = false;
                        }
                        break;
                    case "eyr":
                        int expYear = Integer.parseInt(passport.getCredentials().get(field));
                        isValid = isValid && passport.getCredentials().get(field).length() == 4;
                        if (expYear < 2020 || expYear > 2030)
                        {
                            isValid = false;
                        }
                        break;
                    case "hgt":
                        String heightField = passport.getCredentials().get(field);
                        int height = 0;
                        if (heightField.contains("cm") || heightField.contains("in"))
                        {
                            height = Integer.parseInt(passport.getCredentials().get(field).substring(0, passport.getCredentials().get(field).length() - 2));
                            if ( heightField.contains("cm")) {
                                if (height < 150 || height > 193)
                                {
                                    isValid = false;
                                }
                            } else if (heightField.contains("in"))
                            {
                                if (height < 59 || height > 76)
                                {
                                    isValid = false;
                                }
                            }
                        } else {
                            isValid = false;
                        }

                        break;
                    case "hcl":
                        String hairColor = passport.getCredentials().get(field);
                        isValid = isValid && hairColor.matches("^#([a-f0-9]{6})$");
                        break;
                    case "ecl":
                        String eyeColor = passport.getCredentials().get(field);
                        isValid = isValid && validEyeColor.contains(eyeColor);
                        break;
                    case "pid":
                        isValid = isValid && passport.getCredentials().get(field).length() == 9;
                        break;
                    default:
                        break;
                }
                }
            }
            if (isValid)
            {
                validPassports.add(passport);
            }
        }
        return validPassports;
    }

    public static List<Passport> getPassports(List<String> input)
    {
        List<Passport> passports = new ArrayList<Dec4.Passport>();
        Passport entity = new Passport(passportId);
        Map<String, String> credentials = new HashMap<String, String>();
        for (String line : input)
        {
            if (line.isBlank())
            {
                passports.add(entity);
                passportId++;
                entity = new Passport(passportId);
                credentials = new HashMap<String, String>();
                continue;
            }
            String[] temp = line.split(" ");
            for (int index = 0; index < temp.length; index++)
            {
                String[] entry = temp[index].split(":");
                credentials.put(entry[0], entry[1]);
            }
            entity.setCredentials(credentials);

        }
        return passports;
    }

    public static class Passport
    {
        int id;
        Map<String, String> credentials;

        Passport(int id)
        {
            this.id = id;
        }

        Passport(int id, Map<String, String> credentials)
        {
            this.id = id;
            this.credentials = credentials;
        }

        public int getId()
        {
            return id;
        }

        public void setId(int id)
        {
            this.id = id;
        }

        public Map<String, String> getCredentials()
        {
            return credentials;
        }

        public void setCredentials(Map<String, String> credentials)
        {
            this.credentials = credentials;
        }

    }
}
