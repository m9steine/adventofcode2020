package day16;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import utils.InputUtils;

public class Dec16
{
    private static InputUtils helper = new InputUtils();
    public static List<TicketClass> ticketClasses = new ArrayList<TicketClass>();
    public static Ticket myTicket = new Ticket();
    public static List<Ticket> nearbyTickets = new ArrayList<Ticket>();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day16/input.txt");
        initInput(input);
        checkNearbyTickets();
        System.out.println("Part1- " + checkNearbyTickets());
    }

    public static int checkNearbyTickets()
    {
        List<Integer> result = new ArrayList<Integer>();
        for (Ticket ticket:nearbyTickets)
        {
            for (int value : ticket.fields)
            {
                boolean isValueValid = false;
                for (TicketClass ticketClass : ticketClasses)
                {
                    Range range1 = ticketClass.conditions.get(0);
                    Range range2 = ticketClass.conditions.get(1);
                    if ((value >= range1.min && value <= range1.max) || (value >= range2.min && value <= range2.max))
                    {
                        isValueValid = true;
                        break;
                    }
                }
                if (!isValueValid)
                {
                    result.add(value);
                    break;
                }
            }
        }
        return result.stream().mapToInt(v->v).sum();
    }

    public static void initInput(List<String> input)
    {
        boolean isMyTicket = false;
        boolean isNearbyTickets = false;
        for (String line : input)
        {
            if (line.contains("-"))
            {
                isMyTicket = false;
                isNearbyTickets = false;
                String[] ticketClass = line.split(":");
                String name = ticketClass[0].replace(":", "");
                String[] conditions = ticketClass[1].split(" ");
                String cond1[] = conditions[1].split("-");
                String cond2[] = conditions[3].split("-");
                Range range1 = new Range(Integer.parseInt(cond1[0]), Integer.parseInt(cond1[1]));
                Range range2 = new Range(Integer.parseInt(cond2[0]), Integer.parseInt(cond2[1]));
                ticketClasses.add(new TicketClass(name, List.of(range1, range2)));
            }
            else if (line.contains("your"))
            {
                isMyTicket = true;
                isNearbyTickets = false;
            }
            else if (line.contains("nearby"))
            {
                isNearbyTickets = true;
                isMyTicket = false;
            }

            else if (line.contains(",") && isMyTicket)
            {
                myTicket = new Ticket(List.of(line.split(",")).stream().mapToInt(v -> Integer.parseInt(v)).boxed()
                    .collect(Collectors.toList()));

            }
            else if (line.contains(",") && isNearbyTickets)
            {
                nearbyTickets.add(new Ticket(List.of(line.split(",")).stream().mapToInt(v -> Integer.parseInt(v))
                    .boxed().collect(Collectors.toList())));

            }

        }

    }

    public static class Ticket
    {
        List<Integer> fields = new ArrayList<Integer>();

        public Ticket()
        {
        }

        public Ticket(List<Integer> fields)
        {
            this.fields = fields;
        }
    }

    public static class TicketClass
    {
        String name;
        List<Range> conditions;

        public TicketClass(String name, List<Range> conditions)
        {
            this.name = name;
            this.conditions = conditions;
        }
    }

    public static class Range
    {
        int min;
        int max;

        Range(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }

}
