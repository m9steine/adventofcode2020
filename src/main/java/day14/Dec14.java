package day14;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import utils.InputUtils;

public class Dec14
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day14/input.txt");
        System.out.println("Part1: " + maskInput(input));
    }

    public static long maskInput(List<String> input)
    {
        Map<Long, Long> memory = new HashMap<Long, Long>();
        String mask = "";
        long memIndex = 0;
        String[] memBinaryValue = new String[36];
        for (String line : input)
        {
            if (line.startsWith("mask"))
            {
                mask = line.substring(line.indexOf("=")+2, line.length());
            } else if(line.startsWith("mem"))
            {
                memIndex = Long.parseLong(line.substring(line.indexOf("[")+1, line.indexOf("]")));
                memBinaryValue = String.format("%36s", Long.toBinaryString(Long.parseLong(line.substring(line.indexOf("=")+2, line.length())))).replace(' ', '0').split("");
                int maskIndex = 0;
                for(String value : mask.split(""))
                {
                    if (!value.equals("X"))
                    {
                        memBinaryValue[maskIndex] = value;
                    }
                    maskIndex++;
                }
                memory.put(memIndex, Long.parseLong(String.join("", memBinaryValue), 2));
            }
        }
        return memory.values().stream().mapToLong(v->v).sum();
    }

    public static long maskInput2(List<String> input)
    {
        // For now just a copy of Part1 - method
        Map<Long, Long> memory = new HashMap<Long, Long>();
        String mask = "";
        long memIndex = 0;
        String[] memBinaryValue = new String[36];
        for (String line : input)
        {
            if (line.startsWith("mask"))
            {
                mask = line.substring(line.indexOf("=")+2, line.length());
            } else if(line.startsWith("mem"))
            {
                memIndex = Long.parseLong(line.substring(line.indexOf("[")+1, line.indexOf("]")));
                memBinaryValue = String.format("%36s", Long.toBinaryString(Long.parseLong(line.substring(line.indexOf("=")+2, line.length())))).replace(' ', '0').split("");
                int maskIndex = 0;
                for(String value : mask.split(""))
                {
                    if (!value.equals("X"))
                    {
                        memBinaryValue[maskIndex] = value;
                    }
                    maskIndex++;
                }
                memory.put(memIndex, Long.parseLong(String.join("", memBinaryValue), 2));
            }
        }
        return memory.values().stream().mapToLong(v->v).sum();
    }


}
