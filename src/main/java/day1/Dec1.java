package day1;

import java.util.List;

import utils.InputUtils;

public class Dec1
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day1/input.txt");

        System.out.println("Part1: " + calcPartOne(input));
        System.out.println("Part2: " + calcPartTwo(input));

    }

    public static int calcPartOne(List<String> input)
    {
        for (int ind = 0; ind < input.size() - 1; ind++)
        {
            int x = Integer.parseInt(input.get(ind));
            for (int jnd = 0; jnd < input.size(); jnd++)
            {
                int y = Integer.parseInt(input.get(jnd));
                if (x + y == 2020)
                {
                    return x * y;
                }
            }
        }
        return -1;
    }

    public static int calcPartTwo(List<String> input)
    {
        for (int ind = 0; ind < input.size() - 1; ind++)
        {
            int x = Integer.parseInt(input.get(ind));
            for (int jnd = 0; jnd < input.size(); jnd++)
            {
                int y = Integer.parseInt(input.get(jnd));
                for (int knd = 0; knd < input.size(); knd++)
                {
                    int z = Integer.parseInt(input.get(knd));
                    if (x + y + z == 2020)
                    {
                        return x * y * z;
                    }
                }
            }
        }
        return -1;
    }

}
