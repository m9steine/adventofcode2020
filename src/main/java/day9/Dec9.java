package day9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.InputUtils;

public class Dec9
{
    private static InputUtils helper = new InputUtils();
    private static long[] inputLong;

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day9/input.txt");
        inputLong = initInput(input).stream().mapToLong(v -> v).toArray();

        for (int index = 25; index < inputLong.length; index++)
        {
            if (!isSumOfPrevious(inputLong[index], index))
            {
                System.out.println("Part1 - " + inputLong[index]);
            }
        }
        System.out.println("Part2 - " + getSequence().stream().mapToLong(v -> v).min().getAsLong()+getSequence().stream().mapToLong(v -> v).max().getAsLong());
    }

    public static boolean isSumOfPrevious(long value, int indexValue)
    {
        boolean result = false;
        for (int index = indexValue - 25; index < indexValue; index++)
        {
            long num1 = inputLong[index];
            for (int jndex = indexValue - 25; jndex < indexValue; jndex++)
            {
                long num2 = inputLong[jndex];
                if (inputLong[index] != inputLong[jndex])
                {
                    result = num1 + num2 == value;
                    if (result)
                    {
                        return result;
                    }
                }
            }
        }
        return result;
    }

    public static List<Long> getSequence()
    {
        long checkLong = 23278925;
        long sum = 0;
        List<Long> result = new ArrayList<Long>();
        for (int index = 0; index < inputLong.length; index++)
        {
            sum = 0;
            result = new ArrayList<Long>();
            sum += inputLong[index];
            result.add(inputLong[index]);
            for (int jndex = index + 1; jndex < inputLong.length; jndex++)
            {
                sum += inputLong[jndex];
                result.add(inputLong[jndex]);
                if (sum == checkLong)
                {
                    return result;
                }
            }
        }
        return new ArrayList<Long>();
    }

    public static List<Long> initInput(List<String> input)
    {
        List<Long> output = new ArrayList<Long>();
        for (String line : input)
        {
            output.add(Long.parseLong(line));
        }
        return output;
    }

}
