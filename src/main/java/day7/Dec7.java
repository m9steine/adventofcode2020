package day7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.InputUtils;

public class Dec7
{
    private static InputUtils helper = new InputUtils();
    private static final String SHINYGOLD = "shiny gold";

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day7/input.txt");

        List<String> bagsToFind = new ArrayList<String>();
        bagsToFind.add(SHINYGOLD);
        System.out.println(getBagsIncludingShinyGoldBags(input, new ArrayList<String>(), bagsToFind));
        List<Integer> numberOfBags = new ArrayList<Integer>();
        numberOfBags.add(1);
        System.out.println(getBagsShinyGoldCanInclude(initBags(input), SHINYGOLD));
        System.out.println(initBags(input));
    }

    public static int getBagsIncludingShinyGoldBags(List<String> input, List<String> bagsFound, List<String> bagsToFind)
    {
        List<String> bagsIncludingBagsToFind = new ArrayList<String>();
        if (bagsToFind.size() == 0)
        {
            return bagsFound.size();
        }
        for (String bag : bagsToFind)
        {
            for (String line : input)
            {
                if (line.contains(bag) && !line.startsWith(bag))
                {
                    String bagToAdd = line.substring(0, line.indexOf(" ", line.indexOf(" ") + 1));
                    if (!bagsIncludingBagsToFind.contains(bagToAdd))
                    {
                        if (!bagsFound.contains(bagToAdd))
                        {
                            bagsFound.add(bagToAdd);
                        }
                        bagsIncludingBagsToFind.add(bagToAdd);
                    }
                }
            }

        }
        return getBagsIncludingShinyGoldBags(input, bagsFound, bagsIncludingBagsToFind);
    }

    public static int getBagsShinyGoldCanInclude(List<Bag> bags, String bagName)
    {
        Bag bagToCheck = bags.stream().filter(bag -> bag.name.equals(bagName)).findFirst().orElse(null);
        int result = 1;
        if (bagToCheck != null)
        {
            for (Map.Entry<Bag, Integer> childBag : bagToCheck.includingBags.entrySet())
            {
                result += getBagsShinyGoldCanInclude(bags, childBag.getKey().name) * childBag.getValue();
            }
        }
        return result;
    }

    public static List<Bag> initBags(List<String> input)
    {
        List<Bag> bags = new ArrayList<Bag>();
        for (String line : input)
        {
            String bagName = line.substring(0, line.indexOf(" ", line.indexOf(" ") + 1));
            Bag bag = new Bag(bagName);
            String includingBags = line.split("contain")[1].trim();
            Map<Bag, Integer> children = new HashMap<Bag, Integer>();
            for (String split : includingBags.split(","))
            {
                split = split.trim();
                if (split.contains("no other"))
                {
                    continue;
                }
                int holdingBags = Integer.parseInt(split.substring(0, split.indexOf(" ")));
                String holdingBagName = split.substring(split.indexOf(" ") + 1, split.indexOf("bag") - 1);
                children.put(new Bag(holdingBagName), holdingBags);
            }
            bag.includingBags = children;
            bags.add(bag);
        }
        return bags;
    }

    public static class Bag
    {
        String name;
        Map<Bag, Integer> includingBags;

        public Bag(String name)
        {
            this.name = name;
        }
    }

}
