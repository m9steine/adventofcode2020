package day6;

import java.util.ArrayList;
import java.util.List;

import utils.InputUtils;

public class Dec6
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day6/input.txt");
        System.out.println("Part1 - count Answers: " + countAnswers(getQuestionGroups(input)));
        System.out.println("Part2 - count Answers: " + countAnswersPartTwo(getQuestionGroups(input)));

    }

    public static int countAnswers(List<Group> groups)
    {
        List<String> uniqueAnswers = new ArrayList<String>();
        List<Integer> counts = new ArrayList<Integer>();
        for (Group entity : groups)
        {
            for (String answer : entity.answers)
            {
                    for (String test : answer.split(""))
                    {
                        if (!uniqueAnswers.contains(test))
                        {
                            uniqueAnswers.add(test);
                        }
                    }
            }
            counts.add(uniqueAnswers.size());
            uniqueAnswers = new ArrayList<String>();
        }
        return counts.stream().mapToInt(v -> v).sum();
    }

    public static int countAnswersPartTwo(List<Group> groups)
    {
        for (Group entity : groups)
        {
            List<String> solitryAnswers = new ArrayList<String>();
            for (String answer : entity.answers)
            {
                    for (String test : answer.split(""))
                    {
                        solitryAnswers.add(test);
                    }
            }
            entity.solitaryAnswers = solitryAnswers;
        }

        int count = 0;
        for (Group entity : groups)
        {
            if (entity.numberOfPersons == 1)
            {
                count += entity.solitaryAnswers.size();
            }
            else
            {
                List<String> omg = new ArrayList<String>();
                for (String answer : entity.solitaryAnswers)
                {

                    if (entity.numberOfPersons == (entity.solitaryAnswers.stream()
                        .filter(element -> element.equals(answer)).count()))
                    {
                        if (!omg.contains(answer))
                        {
                            omg.add(answer);
                        }
                    }
                }
                count += omg.size();
            }
        }

        return count;

    }

    public static List<Group> getQuestionGroups(List<String> input)
    {
        List<Group> questions = new ArrayList<Group>();
        int countPeople = 0;
        List<String> answers = new ArrayList<String>();
        for (String line : input)
        {
            if (line.isBlank())
            {
                questions.add(new Group(countPeople, answers));
                countPeople = 0;
                answers = new ArrayList<String>();
                continue;
            }
            countPeople++;
            answers.add(line);

        }
        return questions;
    }

    public static class Group
    {
        int numberOfPersons;
        List<String> answers;
        List<String> solitaryAnswers;

        public List<String> getSolitaryAnswers()
        {
            return solitaryAnswers;
        }

        public void setSolitaryAnswers(List<String> solitaryAnswers)
        {
            this.solitaryAnswers = solitaryAnswers;
        }

        public Group(int countPeople, List<String> answers2)
        {
            this.numberOfPersons = countPeople;
            this.answers = answers2;
        }

    }

}
