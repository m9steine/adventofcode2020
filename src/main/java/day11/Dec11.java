package day11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import utils.InputUtils;

public class Dec11
{
    private static InputUtils helper = new InputUtils();
    private static String[][] initialInput;

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day11/input.txt");
        initialInput = initInput(input);

        System.out.println("Part1:- " + countOccupiedSeats(seatPeople(initialInput)));
        System.out.println("Part2:- " + countOccupiedSeats(seatPeople2(initialInput)));
    }

    public static String[][] initInput(List<String> input)
    {
        String[][] seatingArea = new String[input.size()][input.get(0).length()];
        for (int index = 0; index < input.size(); index++)
        {
            String[] words = input.get(index).split("");
            for (int jndex = 0; jndex < words.length; jndex++)
            {
                seatingArea[index][jndex] = words[jndex];
            }
        }
        return seatingArea;
    }

    public static void printInput(String[][] seatingArea)
    {
        for (int index = 0; index < seatingArea.length; index++)
        {
            for (int jndex = 0; jndex < seatingArea[index].length; jndex++)
            {
                System.out.print(seatingArea[index][jndex]);
            }
            System.out.println();
        }
    }

    public static String[][] seatPeople(String[][] input)
    {
        String[][] output = deepCopyIntMatrix(input);
        for (int index = 0; index < input.length; index++)
        {
            for (int jndex = 0; jndex < input[index].length; jndex++)
            {
                if (!input[index][jndex].equals("."))
                {
                    output[index][jndex] = checkAdjacentSeats(input, index, jndex);
                }
                else
                {
                    output[index][jndex] = ".";
                }
            }
        }
        if (Arrays.deepEquals(input, output))
        {
            return output;
        }
        else
        {
            return seatPeople(output);
        }
    }

    public static String[][] seatPeople2(String[][] input)
    {
        String[][] output = deepCopyIntMatrix(input);
        for (int index = 0; index < input.length; index++)
        {
            for (int jndex = 0; jndex < input[index].length; jndex++)
            {
                if (!input[index][jndex].equals("."))
                {
                    output[index][jndex] = checkAdjacentRowSeats(input, index, jndex);
                }
                else
                {
                    output[index][jndex] = ".";
                }
            }
        }
        if (Arrays.deepEquals(input, output))
        {
            return output;
        }
        else
        {
            printInput(output);
            System.out.println();
            return seatPeople2(output);
        }
    }

    private static String checkAdjacentSeats(String[][] input, int index, int jndex)
    {
        int occupiedSeats = 0;
        if (index == 0)
        {
            if (jndex == 0)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1], input[index + 1][jndex], input[index + 1][jndex + 1])
                        .filter(el -> (el.equals("#"))).count();
            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1], input[index][jndex - 1], input[index + 1][jndex + 1],
                        input[index + 1][jndex], input[index + 1][jndex - 1]).filter(el -> el.equals("#")).count();

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex - 1], input[index + 1][jndex], input[index + 1][jndex - 1])
                        .filter(el -> el.equals("#")).count();
            }
        }
        else if (index < input.length - 1)
        {
            if (jndex == 0)
            {

                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1], input[index + 1][jndex + 1],
                        input[index + 1][jndex], input[index - 1][jndex + 1], input[index - 1][jndex])
                        .filter(el -> el.equals("#")).count();

            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1], input[index + 1][jndex + 1],
                        input[index + 1][jndex], input[index - 1][jndex + 1], input[index - 1][jndex],
                        input[index - 1][jndex - 1], input[index][jndex - 1],
                        input[index + 1][jndex - 1]).filter(el -> el.equals("#")).count();

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(
                        input[index + 1][jndex], input[index - 1][jndex], input[index - 1][jndex - 1],
                        input[index][jndex - 1],
                        input[index + 1][jndex - 1]).filter(el -> el.equals("#")).count();
            }
        }
        else if (index == input.length - 1)
        {
            if (jndex == 0)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1],
                        input[index - 1][jndex + 1], input[index - 1][jndex]).filter(el -> el.equals("#")).count();

            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(input[index][jndex + 1],
                        input[index - 1][jndex + 1], input[index - 1][jndex], input[index][jndex - 1],
                        input[index - 1][jndex - 1]).filter(el -> el.equals("#")).count();

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats =
                    (int) Stream.of(input[index - 1][jndex], input[index - 1][jndex], input[index - 1][jndex - 1])
                        .filter(el -> el.equals("#")).count();
            }
        }
        if (occupiedSeats == 0)
        {
            return "#";
        }
        else if (occupiedSeats >= 4)
        {
            return "L";
        }
        return input[index][jndex];
    }

    private static String checkAdjacentRowSeats(String[][] input, int index, int jndex)
    {
        int occupiedSeats = 0;
        if (index == 0)
        {
            if (jndex == 0)
            {
                occupiedSeats = checkRight(input, index, jndex) + checkDown(input, index, jndex)
                    + checkRightDown(input, index, jndex);
            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats = checkRight(input, index, jndex) + checkRightDown(input, index, jndex)
                    + checkDown(input, index, jndex) + checkLeftDown(input, index, jndex)
                    + checkLeft(input, index, jndex);

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats = checkLeft(input, index, jndex) + checkLeftDown(input, index, jndex)
                    + checkDown(input, index, jndex);
            }
        }
        else if (index < input.length - 1)
        {
            if (jndex == 0)
            {

                occupiedSeats =
                    checkUp(input, index, jndex) + checkRightUp(input, index, jndex) + checkRight(input, index, jndex)
                        + checkRightDown(input, index, jndex) + checkDown(input, index, jndex);

            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats = checkLeft(input, index, jndex) + checkLeftDown(input, index, jndex)
                    + checkLeftUp(input, index, jndex) + checkDown(input, index, jndex) + checkUp(input, index, jndex)
                    + checkRight(input, index, jndex) + checkRightDown(input, index, jndex)
                    + checkRightUp(input, index, jndex);

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats =
                    checkLeft(input, index, jndex) + checkLeftUp(input, index, jndex) + checkUp(input, index, jndex)
                        + checkLeftDown(input, index, jndex) + checkDown(input, index, jndex);
            }
        }
        else if (index == input.length - 1)
        {
            if (jndex == 0)
            {
                occupiedSeats = checkRight(input, index, jndex) + checkRightUp(input, index, jndex)
                    + checkUp(input, index, jndex);

            }
            else if (jndex < input[index].length - 1)
            {
                occupiedSeats =
                    checkUp(input, index, jndex) + checkLeftUp(input, index, jndex) + checkLeft(input, index, jndex)
                        + checkRightUp(input, index, jndex) + checkRight(input, index, jndex);

            }
            else if (jndex == input[index].length - 1)
            {
                occupiedSeats =
                    checkUp(input, index, jndex) + checkLeft(input, index, jndex) + checkLeftUp(input, index, jndex);
            }
        }
        if (occupiedSeats == 0)
        {
            return "#";
        }
        else if (occupiedSeats >= 5)
        {
            return "L";
        }
        return input[index][jndex];
    }

    private static int checkRightDown(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; true; innerIndex++)
        {
            if (index + innerIndex > input.length - 1 || jndex + innerIndex > input[index].length - 1)
            {
                break;
            }
            String current = input[index + innerIndex][jndex + innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    private static int checkRightUp(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; true; innerIndex++)
        {
            if (index - innerIndex < 0 || jndex + innerIndex > input[index].length - 1)
            {
                break;
            }
            String current = input[index - innerIndex][jndex + innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    private static int checkLeftDown(String[][] input, int index, int jndex)
    {

        for (int innerIndex = 1; true; innerIndex++)
        {
            if (index + innerIndex > input.length - 1 || jndex - innerIndex < 0)
            {
                break;
            }
            String current = input[index + innerIndex][jndex - innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    private static int checkLeftUp(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; true; innerIndex++)
        {
            if (index - innerIndex < 0 || jndex - innerIndex < 0)
            {
                break;
            }
            String current = input[index - innerIndex][jndex - innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    private static int checkDown(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; index + innerIndex < input.length; innerIndex++)
        {
            String current = input[index + innerIndex][jndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;

    }

    private static int checkUp(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; index - innerIndex >= 0; innerIndex++)
        {
            String current = input[index - innerIndex][jndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;

    }

    private static int checkRight(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; jndex + innerIndex < input[index].length; innerIndex++)
        {
            String current = input[index][jndex + innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    private static int checkLeft(String[][] input, int index, int jndex)
    {
        for (int innerIndex = 1; jndex - innerIndex >= 0; innerIndex++)
        {
            String current = input[index][jndex - innerIndex];
            if (current.equals("L"))
            {
                return 0;
            }
            else if (current.equals("#"))
            {
                return 1;
            }
        }
        return 0;
    }

    public static int countOccupiedSeats(String[][] input)
    {
        int sum = 0;
        for (int index = 0; index < input.length; index++)
        {
            for (int jndex = 0; jndex < input[index].length; jndex++)
            {
                if (input[index][jndex].equals("#"))
                {
                    sum++;
                }
            }
        }
        return sum;
    }

    public static String[][] deepCopyIntMatrix(String[][] input)
    {
        if (input == null)
            return null;
        String[][] result = new String[input.length][];
        for (int r = 0; r < input.length; r++)
        {
            result[r] = input[r].clone();
        }
        return result;
    }

}
