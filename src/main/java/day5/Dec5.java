package day5;

import java.util.ArrayList;
import java.util.List;

import utils.InputUtils;

public class Dec5
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day5/input.txt");
        List<Integer> seatIds = new ArrayList<Integer>();
        int maxSeatId = 0;
        int rows = 128;
        int cols = 8;
        for (String line : input)
        {
            int seatId =
                getSeatId(getRowOrCol(line.substring(0, 7), rows), getRowOrCol(line.substring(7, line.length()), cols));
            seatIds.add(seatId);
            maxSeatId = seatId > maxSeatId ? seatId : maxSeatId;
        }
        System.out.println("MaxSeatId: " + seatIds.stream().mapToInt(ele -> ele).max().orElse(-1));
        System.out.println("Missing Value : "
            + identifyMissingValues((seatIds.stream().sorted().mapToInt(Integer::intValue).toArray())));

    }

    public static int getRowOrCol(String input, int max)
    {
        int start = 1;
        int end = max;
        for (int index = 0; index < input.length(); index++)
        {
            switch (input.charAt(index))
            {
                // B or R
                case 66:
                case 82:
                    start = start + (max / (int) Math.pow(2, index + 1));

                    break;
                // F or L
                case 70:
                case 76:
                    if (index == 0 || (index > 0 && input.charAt(index - 1) == 70))
                    {
                        end = end / 2;
                    }
                    else
                    {
                        end = start + (start / (int) Math.pow(2, index + 1));

                    }

                    break;
            }

        }
        return start - 1;
    }

    public static int getSeatId(int row, int col)
    {
        return row * 8 + col;
    }

    private static int identifyMissingValues(int[] ar)
    {
        for (int i = 0; i < (ar.length - 1); i++)
        {
            int next = ar[i + 1];
            int current = ar[i];
            if ((next - current) > 1)
            {
                for (int ind = 1; ind < next - current; ind++)
                    return current + ind;
            }
        }
        return -1;
    }

}
