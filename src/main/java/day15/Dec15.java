package day15;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import utils.InputUtils;

public class Dec15
{
    private static InputUtils helper = new InputUtils();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day15/input.txt");
        List<NumberSpoken> numbersList = new ArrayList<NumberSpoken>();
        Map<Long, NumberSpoken> numbersMap = new HashMap<Long, NumberSpoken>();

        int index = 1;
        for (String number : input.get(0).split(","))
        {
            numbersList.add(new NumberSpoken(Long.parseLong(number), index));
            numbersMap.put(Long.parseLong(number), new NumberSpoken(Long.parseLong(number), index));
            index++;
        }
        System.out.println("Part1: " + getXNumber(numbersList, 2020));
        // not possible with this approach. after 20 min at index 2.000.000
        // System.out.println("Part2: " + getXNumber(numbersList, 30000000));
        // with HashMap in 7 sek.
        System.out.println("Part2: " + getXNumberMap(numbersMap, 30000000));
    }

    public static long getXNumber(List<NumberSpoken> numbers, int finish)
    {
        long index = numbers.size() + 1;
        long lastSpokenNumber = numbers.get((int) (index - 2)).number;
        while (index <= finish)
        {
            long lastNumber = lastSpokenNumber;
            NumberSpoken last = numbers.stream().filter(number -> number.number == lastNumber).findFirst().orElse(null);
            if (!last.firstTimeSpoken)
            {
                NumberSpoken current = numbers.stream().filter(number -> number.number == 0).findFirst().orElse(null);
                if (current != null)
                {
                    current.previouslySpoken = current.turn;
                    current.turn = index;
                    current.firstTimeSpoken = true;
                }
                else
                {
                    numbers.add(new NumberSpoken(0, index));
                }
                lastSpokenNumber = 0;
            }
            else
            {
                NumberSpoken toAdd = new NumberSpoken(last.turn - last.previouslySpoken, index);
                lastSpokenNumber = toAdd.number;

                NumberSpoken checkCurrent =
                    numbers.stream().filter(number -> number.number == toAdd.number).findFirst().orElse(null);
                if (checkCurrent != null)
                {
                    checkCurrent.previouslySpoken = checkCurrent.turn;
                    checkCurrent.turn = index;
                    checkCurrent.firstTimeSpoken = true;
                }
                else
                {

                    numbers.add(toAdd);
                }
            }
            index++;
        }

        return lastSpokenNumber;
    }

    public static long getXNumberMap(Map<Long, NumberSpoken> numbers, int finish)
    {
        long index = numbers.size() + 1;
        long lastSpokenNumber = numbers.get(2L).number;
        while (index <= finish)
        {
            long lastNumber = lastSpokenNumber;
            NumberSpoken last = numbers.get(lastNumber);
            if (!last.firstTimeSpoken)
            {
                NumberSpoken current = numbers.get(0L);
                if (current != null)
                {
                    current.previouslySpoken = current.turn;
                    current.turn = index;
                    current.firstTimeSpoken = true;
                }
                else
                {
                    numbers.put(0L, new NumberSpoken(0, index));
                }
                lastSpokenNumber = 0;
            }
            else
            {
                NumberSpoken toAdd = new NumberSpoken(last.turn - last.previouslySpoken, index);
                lastSpokenNumber = toAdd.number;

                NumberSpoken checkCurrent = numbers.get(toAdd.number);
                if (checkCurrent != null)
                {
                    checkCurrent.previouslySpoken = checkCurrent.turn;
                    checkCurrent.turn = index;
                    checkCurrent.firstTimeSpoken = true;
                }
                else
                {

                    numbers.put(toAdd.number, toAdd);
                }
            }
            index++;
        }

        return lastSpokenNumber;
    }

    public static class NumberSpoken
    {
        long number;
        long turn;
        long previouslySpoken;
        boolean firstTimeSpoken = false;

        public NumberSpoken(long number, long turn)
        {
            this.number = number;
            this.turn = turn;
            this.previouslySpoken = turn;
            // TODO Auto-generated constructor stub
        }
    }

}
