package day12;

import java.util.List;

import utils.InputUtils;

public class Dec12
{
    private static InputUtils helper = new InputUtils();
    private static Ship ship = new Ship();
    private static Ship ship2 = new Ship();
    private static Waypoint waypoint = new Waypoint();

    public static void main(String[] args)
    {
        List<String> input = helper.getInput("day12/input.txt");
        System.out.println("Part1- " + moveShip(input));
        System.out.println("Part2- " + moveShipPart2(input));
    }

    public static int moveShip(List<String> input)
    {
        for (String line : input)
        {
            String instruction = line.substring(0, 1);
            int value = Integer.parseInt(line.substring(1, line.length()));

            switch (instruction)
            {
                case "R":
                    value = value / 90;
                    turnRight(value);
                    break;
                case "L":
                    value = value / 90;
                    turnLeft(value);
                    break;
                case "F":
                    moveForward(value);
                    // calcWaypoint();
                    break;
                default:
                    moveDirection(instruction, value);
                    break;
            }
        }

        return Math.abs(ship.eastWest) + Math.abs(ship.northSouth);
    }

    public static int moveShipPart2(List<String> input)
    {
        for (String line : input)
        {
            String instruction = line.substring(0, 1);
            int value = Integer.parseInt(line.substring(1, line.length()));

            switch (instruction)
            {
                case "R":
                    value = value / 90;
                    turnWaypointRight(value);
                    break;
                case "L":
                    value = value / 90;
                    turnWaypointLeft(value);
                    break;
                case "F":
                    moveForwardToWaypoint(value);
                    break;
                default:
                    moveDirectionOfWaypoint(instruction, value);
                    break;
            }
        }

        return Math.abs(ship2.eastWest) + Math.abs(ship2.northSouth);
    }

    public static void turnRight(int changeDirection)
    {
        for (int index = 0; index < changeDirection; index++)
        {
            switch (ship.direction)
            {
                case EAST:
                    ship.direction = Direction.SOUTH;
                    break;
                case SOUTH:
                    ship.direction = Direction.WEST;
                    break;
                case WEST:
                    ship.direction = Direction.NORTH;
                    break;
                case NORTH:
                    ship.direction = Direction.EAST;
                    break;
                default:
                    break;
            }
        }
    }

    public static void turnWaypointRight(int changeDirection)
    {
        int initEastWest = waypoint.eastWest;
        int initNorthSouth = waypoint.northSouth;
        switch (changeDirection)
        {
            // 90 degree
            case 1:
                waypoint.eastWest = initNorthSouth;
                waypoint.northSouth = initEastWest * -1;
                break;
            case 2:
                waypoint.eastWest = initEastWest * -1;
                waypoint.northSouth = initNorthSouth * -1;
                break;
            case 3:
                waypoint.eastWest = initNorthSouth * -1;
                waypoint.northSouth = initEastWest;
                break;
            default:
                break;
        }
    }

    public static void turnLeft(int changeDirection)
    {
        for (int index = 0; index < changeDirection; index++)
        {
            switch (ship.direction)
            {
                case EAST:
                    ship.direction = Direction.NORTH;
                    break;
                case SOUTH:
                    ship.direction = Direction.EAST;
                    break;
                case WEST:
                    ship.direction = Direction.SOUTH;
                    break;
                case NORTH:
                    ship.direction = Direction.WEST;
                    break;
                default:
                    break;
            }
        }
    }

    public static void turnWaypointLeft(int changeDirection)
    {
        int initEastWest = waypoint.eastWest;
        int initNorthSouth = waypoint.northSouth;
        switch (changeDirection)
        {
            // 90 degree
            case 1:
                waypoint.eastWest = initNorthSouth * -1;
                waypoint.northSouth = initEastWest;
                break;
            case 2:
                waypoint.eastWest = initEastWest * -1;
                waypoint.northSouth = initNorthSouth * -1;
                break;
            case 3:
                waypoint.eastWest = initNorthSouth;
                waypoint.northSouth = initEastWest * -1;
                break;
            default:
                break;
        }
    }

    public static void moveForward(int value)
    {
        switch (ship.direction)
        {
            case EAST:
                ship.eastWest += value;
                break;
            case SOUTH:
                ship.northSouth -= value;
                break;
            case WEST:
                ship.eastWest -= value;
                break;
            case NORTH:
                ship.northSouth += value;
                break;
            default:
                break;
        }
    }

    public static void moveForwardToWaypoint(int value)
    {
        ship2.eastWest += value * waypoint.eastWest;
        ship2.northSouth += value * waypoint.northSouth;
    }

    public static void moveDirection(String direction, int value)
    {
        switch (direction)
        {
            case "E":
                ship.eastWest += value;
                break;
            case "S":
                ship.northSouth -= value;
                break;
            case "W":
                ship.eastWest -= value;
                break;
            case "N":
                ship.northSouth += value;
                break;
            default:
                break;
        }
    }

    public static void moveDirectionOfWaypoint(String direction, int value)
    {
        switch (direction)
        {
            case "E":
                waypoint.eastWest += value;
                break;
            case "S":
                waypoint.northSouth -= value;
                break;
            case "W":
                waypoint.eastWest -= value;
                break;
            case "N":
                waypoint.northSouth += value;
                break;
            default:
                break;
        }
    }

    public static class Ship
    {
        int eastWest = 0;
        int northSouth = 0;
        Direction direction = Direction.EAST;
    }

    public static class Waypoint
    {
        int eastWest = 10;
        int northSouth = 1;
    }

    public static enum Direction
    {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }
}
