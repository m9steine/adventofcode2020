package day4;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import utils.InputUtils;

public class TestDec4
{

    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day4/test.txt");
    }

    @Test
    public void testGetPassports()
    {
        List<Passport> passports = Dec4.getPassports(input);

        assertEquals(4, passports.size());
    }

    @Test
    public void testGetValidPassports()
    {
        List<Passport> passports = Dec4.getPassports(input);
        List<Passport> validPassports = Dec4.getValidPassports(passports);

        assertEquals(2, validPassports.size());
    }


}
