package day10;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec10
{

    private InputUtils helper = new InputUtils();
    private long[] inputLong;
    List<String> input;
    List<String> input2;
    List<String> input3;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day10/test.txt");
        this.input2 = helper.getInput("day10/test2.txt");
        this.input3 = helper.getInput("day10/test3.txt");
    }

    @Test
    public void testSum()
    {
        System.out.println(Dec10.checkJolts(Dec10.initInput(input)));
        System.out.println(Dec10.checkJolts(Dec10.initInput(input2)));
    }

    @Test
    public void testSumddd()
    {
        Dec10.countArrangemnts(Dec10.initInput(input));
        Dec10.checkJolts(Dec10.initInput(input));
        Dec10.countArrangemnts(Dec10.initInput(input2));
        Dec10.checkJolts(Dec10.initInput(input2));
        Dec10.countArrangemnts(Dec10.initInput(input3));
        Dec10.checkJolts(Dec10.initInput(input3));
    }

}
