package day11;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec11
{

    private InputUtils helper = new InputUtils();
    private static String[][] initialInput;
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day11/test.txt");
        this.initialInput = Dec11.initInput(input);
    }

    @Test
    public void testSum()
    {
        Dec11.printInput(initialInput);
        System.out.println(Dec11.countOccupiedSeats(Dec11.seatPeople(initialInput)));
    }
    
    @Test
    public void testSum2()
    {
        Dec11.printInput(initialInput);
        System.out.println(Dec11.countOccupiedSeats(Dec11.seatPeople2(initialInput)));
    }

}
