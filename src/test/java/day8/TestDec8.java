package day8;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec8
{

    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day8/test.txt");
    }

    @Test
    public void testCalcAcc()
    {
        System.out.println(Dec8.calculateAccumulator(Dec8.initOperationList(input)));
    }

    @Test
    public void testCountOps()
    {
        assertEquals(4, Dec8.countOperations(Dec8.initOperationList(input)));
    }
    
    
    @Test
    public void testInifinit() {
        int countOperationsNopJmp = Dec8.countOperations(Dec8.initOperationList(input));
        for (int index = 1; index <= countOperationsNopJmp; index++)
        {
            int test = index;
            new Thread(() -> {
                System.out.println(Dec8.calculateAccumulatorPart2(Dec8.initOperationList(input), test));
                // Do whatever
            }).start();
        }
    }

}
