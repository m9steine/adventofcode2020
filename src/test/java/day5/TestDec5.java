package day5;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import utils.InputUtils;

public class TestDec5
{

    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day5/test.txt");
    }

    @Test
    public void testGetRow()
    {
        for(String line: input)
        {
            System.out.println(Dec5.getRowOrCol(line.substring(0, 7), 128));
        }
    }

    @Test
    public void testGetCol()
    {
        for(String line: input)
        {
            System.out.println(Dec5.getRowOrCol(line.substring(7,line.length()), 8));
        }
    }

    @Test
    public void testGetSeatId()
    {
        for(String line: input)
        {
            System.out.println("seatID:" + Dec5.getSeatId(Dec5.getRowOrCol(line.substring(0, 7), 128), Dec5.getRowOrCol(line.substring(7,line.length()), 8)));
        }
    }


}
