package day12;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec12
{

    private InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day12/test.txt");
    }

    @Test
    public void testPart1()
    {
        assertEquals(25, Dec12.moveShip(input));
    }

    @Test
    public void testPart2()
    {
        assertEquals(286, Dec12.moveShipPart2(input));
    }

}
