package day2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import utils.InputUtils;

public class TestDec2
{
    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day2/test.txt");
    }

    @Test
    public void testGetNumValidPasswords()
    {
        assertEquals(2, Dec2.getNumValidPasswords(input));
    }

    @Test
    public void testCountCheckInPassword()
    {
        assertEquals(1, Dec2.getNumValidPasswordsPartTwo(input));
    }

}
