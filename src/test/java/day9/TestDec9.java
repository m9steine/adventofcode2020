package day9;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec9
{

    private InputUtils helper = new InputUtils();
    private long[] inputLong;
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day9/test.txt");
    }
    
    @Test
    public void testSum()
    {
        inputLong = Dec9.initInput(input).stream().mapToLong(v->v).toArray();

        for (int index = 5; index < inputLong.length; index++)
        {
            if (Dec9.isSumOfPrevious(inputLong[index], index))
            {
                System.out.println(inputLong[index]);
            }
        }
    }


}
