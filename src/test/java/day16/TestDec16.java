package day16;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day15.Dec15.NumberSpoken;
import utils.InputUtils;

public class TestDec16
{

    private InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day16/test.txt");
    }

    @Test
    public void testPart1()
    {
       Dec16.initInput(input);
    }

    @Test
    public void testPart2()
    {
    }

}
