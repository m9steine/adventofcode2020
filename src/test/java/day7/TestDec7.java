package day7;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import day7.Dec7.Bag;
import utils.InputUtils;

public class TestDec7
{

    private static InputUtils helper = new InputUtils();
    List<String> input;
    List<String> inputTwo;
    private static final String SHINYGOLD = "shiny gold";

    @Before
    public void startup()
    {
        this.input = helper.getInput("day7/test.txt");
        this.inputTwo = helper.getInput("day7/test2.txt");
    }

    @Test
    public void testGetBagsIncludingShinyGoldBags()
    {
        List<String> bagsToFind = new ArrayList<String>();
        bagsToFind.add(SHINYGOLD);
        System.out.println(Dec7.getBagsIncludingShinyGoldBags(input,new ArrayList<String>(), bagsToFind));
    }

    @Test
    public void testGetBagsShinyGoldBagsInclude()
    {
        List<String> bagsToFind = new ArrayList<String>();
        bagsToFind.add(SHINYGOLD);
        List<Integer> numberOfBags = new ArrayList<Integer>();
        numberOfBags.add(1);
        assertEquals(32, Dec7.getBagsShinyGoldCanInclude(Dec7.initBags(input), SHINYGOLD) - 1);
        assertEquals(126, Dec7.getBagsShinyGoldCanInclude(Dec7.initBags(inputTwo), SHINYGOLD) - 1);
    }

    @Test
    public void testInitBags()
    {
        List<Bag> bags= new ArrayList<Bag>();
        for(Bag bag : Dec7.initBags(input))
        {
            bags.add(bag);
        }
        assertEquals(9, bags.size());
    }


}
