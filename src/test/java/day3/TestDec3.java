package day3;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import utils.InputUtils;

public class TestDec3
{

    private static InputUtils helper = new InputUtils();
    List<String> input;
    char[][] grid = new char[400][400];

    @Before
    public void startup()
    {
        this.input = helper.getInput("day3/test.txt");
        grid = Dec3.initGrid(input);
        Dec3.printGrid(grid);
    }

    @Test
    public void testPartOne()
    {
        assertEquals(7, Dec3.checkPosition(0, 0, grid, 0, 3, 1));
    }

    @Test
    public void testPartTwo()
    {
        assertEquals(336, (
            Dec3.checkPosition(0, 0, grid, 0, 1, 1)*
            Dec3.checkPosition(0, 0, grid, 0, 3, 1)*
            Dec3.checkPosition(0, 0, grid, 0, 5, 1)*
            Dec3.checkPosition(0, 0, grid, 0, 7, 1)*
            Dec3.checkPosition(0, 0, grid, 0, 1, 2)));
    }

}
