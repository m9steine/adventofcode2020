package day6;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day4.Dec4.Passport;
import utils.InputUtils;

public class TestDec6
{

    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day6/test.txt");
    }

    @Test
    public void testGetGroups()
    {
        for(Dec6.Group ent : Dec6.getQuestionGroups(input))
        {
            System.out.println(ent.numberOfPersons + "--" + ent.answers);
        }
    }

    @Test
    public void testGetCount()
    {
        System.out.println(Dec6.countAnswers(Dec6.getQuestionGroups(input)));
    }

    @Test
    public void testGetCountPartTwo()
    {
        System.out.println(Dec6.countAnswersPartTwo(Dec6.getQuestionGroups(input)));
    }

}
