package day1;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import utils.InputUtils;

public class TestDec1
{

    private static InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day1/test.txt");
    }

    @Test
    public void testPartOne()
    {
        assertEquals(514579, Dec1.calcPartOne(input));
    }

    @Test
    public void testPartTwo()
    {
        assertEquals(241861950, Dec1.calcPartTwo(input));
    }

}
