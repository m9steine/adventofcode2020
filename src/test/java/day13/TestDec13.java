package day13;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import utils.InputUtils;

public class TestDec13
{

    private InputUtils helper = new InputUtils();
    List<String> input;
    List<String> input2;
    List<String> input3;
    List<String> input4;
    List<String> input5;
    List<String> input6;
    List<String> input7;
    long timestampOfArrival = 0;
    List<Dec13.Bus> buses;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day13/test.txt");
        this.input2 = helper.getInput("day13/test2.txt");
        this.input3 = helper.getInput("day13/test3.txt");
        this.input4 = helper.getInput("day13/test4.txt");
        this.input5 = helper.getInput("day13/test5.txt");
        this.input6 = helper.getInput("day13/test6.txt");
        timestampOfArrival = Long.parseLong(input.get(0));
        buses = Dec13.initBuses(input.get(1));
    }

    @Test
    public void testPart1()
    {
        System.out.println(Dec13.getNextBus(buses, timestampOfArrival));
    }

    @Test
    public void testPart2()
    {
//        assertEquals(1068781, Dec13.getFirstTimestampMatching(Dec13.initBuses(input.get(1))));
//        assertEquals(3417, Dec13.getFirstTimestampMatching(Dec13.initBuses(input2.get(1))));
//        assertEquals(754018, Dec13.getFirstTimestampMatching(Dec13.initBuses(input3.get(1))));
//        assertEquals(779210, Dec13.getFirstTimestampMatching(Dec13.initBuses(input4.get(1))));
//        assertEquals(1261476, Dec13.getFirstTimestampMatching(Dec13.initBuses(input5.get(1))));
//        assertEquals(1202161486, Dec13.getFirstTimestampMatching(Dec13.initBuses(input6.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input2.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input3.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input4.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input5.get(1))));
        System.out.println(Dec13.getFirstTimestampMatching(Dec13.initBuses(input6.get(1))));
    }

}
