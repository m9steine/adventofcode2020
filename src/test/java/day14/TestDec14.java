package day14;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import utils.InputUtils;

public class TestDec14
{

    private InputUtils helper = new InputUtils();
    List<String> input;

    @Before
    public void startup()
    {
        this.input = helper.getInput("day14/test.txt");
    }

    @Test
    public void testPart1()
    {
        Dec14.maskInput(input);
    }

    @Test
    public void testPart2()
    {
    }

}
