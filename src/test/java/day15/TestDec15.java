package day15;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import day15.Dec15.NumberSpoken;
import utils.InputUtils;

public class TestDec15
{

    private InputUtils helper = new InputUtils();
    List<String> input;
    List<NumberSpoken> numbers = new ArrayList<NumberSpoken>();

    @Before
    public void startup()
    {
        this.input = helper.getInput("day15/test.txt");
        int index = 1;
        for (String number : input.get(0).split(",")) {
            numbers.add(new NumberSpoken(Long.parseLong(number), index));
            index++;
        }
    }

    @Test
    public void testPart1()
    {
        Dec15.getXNumber(numbers, 2020);
    }

    @Test
    public void testPart2()
    {
    }

}
